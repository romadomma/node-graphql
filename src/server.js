const express = require("express");
const { postgraphile } = require("postgraphile/build/index");
const filterPlugin = require("postgraphile-plugin-connection-filter");
const { makeExtendSchemaPlugin} = require("graphile-utils");
const getCustomSchema = require(`${__dirname}/schema`);
const cors = require('cors');

const customSchema = makeExtendSchemaPlugin(build => getCustomSchema(build));


const app = express();

app.use(cors());
app.use(
    postgraphile(
        process.env.DATABASE_URL || "postgresql://onpp_user:123123@95.179.241.170:5432/onpp",
        'onpp',
        {
            ignoreRBAC: true,
            appendPlugins: [filterPlugin, customSchema],
            extendedErrors: ['hint', 'detail', 'errcode'],
            graphiql: true,
            enhanceGraphiql: true,
            dynamicJson: true,
            simpleCollections: 'both',
        }
        ));
console.log('OK');
app.listen(process.env.PORT || 3001);
