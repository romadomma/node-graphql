const {gql} = require("graphile-utils");

const calcParkingTime = (timeArr, timeDep) => Math.floor((timeDep ? (new Date(timeDep) - new Date(timeArr)) : (new Date() - new Date(timeArr))) / 1000 / 60);
const calcPayTime = (timeArr, timeDep, freeTime) => {
        const allTime = calcParkingTime(timeArr, timeDep);
        return allTime > freeTime ? allTime - freeTime : 0;
    };
const calcPay = (timeArr, timeDep, freeTime, cost) => calcPayTime(timeArr, timeDep, freeTime) * cost;

module.exports = function getCustomSchema(build) {
    return {
        typeDefs: gql`
        extend type PayCalc {  
                "Платное время"    
                payTime: Int! @requires(columns: ["time_arr", "time_dep", "free_time"])
                "Все время на парковке"      
                parkingTime: Int! @requires(columns: ["time_arr", "time_dep"])
                "не должен нам денег"      
                exitCheck: Boolean! @requires(columns: ["success", "timeArr", "timeDep", "freeTime", "cost"])   
                "Сколько уже должен"   
                pay: Int! @requires(columns: ["time_arr", "time_dep", "free_time", "cost"])
                pushEventsJson: [PushEvent!]   @requires(columns: ["push_events_json"])  
        }
        
        type PushEvent{
            id: Int!
            "Тип уведомления"
            type: Int
            check_in: Int!
        }
        `,
        resolvers: {
            PayCalc: {
                pushEventsJson: ({pushs}) => pushs,
                parkingTime: ({timeArr, timeDep}) => calcParkingTime(timeArr, timeDep),
                payTime: ({timeArr, timeDep, freeTime}) => calcPayTime(timeArr, timeDep, freeTime),
                exitCheck: ({success, timeArr, timeDep,  freeTime, cost}) => (!!success || calcPay(timeArr, timeDep, freeTime, cost) <= 0),
                pay: ({timeArr, timeDep, freeTime, cost}) => calcPay(timeArr, timeDep, freeTime, cost),
            },
        }
    }
};

